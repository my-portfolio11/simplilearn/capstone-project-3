# Spring Boot Web App Deployment

## CI Pipeline

[![pipeline status](https://gitlab.com/my-portfolio11/simplilearn/capstone-project-3/badges/main/pipeline.svg)](https://gitlab.com/my-portfolio11/simplilearn/capstone-project-3/-/commits/main)

## Build on AWS EC2

[How to Build](https://gitlab.com/my-portfolio11/simplilearn/capstone-project-3/-/blob/main/notes/Build%20Instructions.pdf)

## Deploy on Docker

1. Change to the `docker` folder
2. Replace the MySQL root password in `.env` with any strong password.
3. Try to understand the `compose.yml` file.
4. Run this to build the image and bring up the services (containers):

```
docker compose pull
docker compose up -d
```

4. Once you are done, run this to bring it down:

```
docker compose down
```
## Gitlab CI

1. Check the file `.gitlab-ci.yml`

    - stages: build, test, deploy
    - jobs: maven-build, maven-test, deploy-to-gitlab-registry, 

2. Settings > CI/CD > Auto Devops > Disable and save.
2. Go to Settings > CI/CD > Variables > Add variables.

    - AWS_ACCESS_KEY_ID
    - AWS_SECRET_ACCESS_KEY
    - AWS_DEFAULT_REGION
    - MYSQL_ROOT_PASS
    - MYSQL_DB
    - MYSQL_USER
    - MYSQL_PASS
    - TAG
    
3. When you commit a change to the master branch, a new pipeline will be run in `CI/CD > Pipelines.`
4. You can also manually run a pipeline in `CI/CD > Pipelines.`
5. The docker image will be built, tagged and pushed to AWS ECR.

# References

Spring Boot CRUD Web application with Pagination and Sorting features using Spring Boot, ThymeLeaf, Spring Data JPA, Hibernate, MySQL database

### Tutorial - Spring Boot CRUD Web Application with Thymeleaf, Spring MVC, Spring Data JPA, Hibernate, MySQL
https://www.javaguides.net/2020/05/spring-boot-crud-web-application-with-thymeleaf.html

### YouTube Video - Spring Boot CRUD Web Application with Thymeleaf, Spring MVC, Spring Data JPA, Hibernate, MySQL
https://youtu.be/_5sAmaRJd2c

### Tutorial - Pagination and Sorting with Spring Boot, ThymeLeaf, Spring Data JPA, Hibernate, MySQL
https://www.javaguides.net/2020/06/pagination-and-sorting-with-spring-boot-thymeleaf-spring-data-jpa-hibernate-mysql.html

### YouTube Video  - Pagination and Sorting with Spring Boot, ThymeLeaf, Spring Data JPA, Hibernate, MySQL
=> https://youtu.be/Aie8n12EFQc
