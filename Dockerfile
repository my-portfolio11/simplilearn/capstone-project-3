# Stage 2: Build final image
# First copy to the repo root and modify application.properties
FROM openjdk:11-jre-slim
ARG jar_file="target/*.jar"
RUN echo ${jar_file}
COPY ${jar_file} /app.jar
EXPOSE 8080
ENTRYPOINT ["sh", "-c", "java -jar /app.jar"]