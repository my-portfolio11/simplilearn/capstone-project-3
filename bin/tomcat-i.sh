#!/bin/bash

# Check if run as root
if [ $EUID -ne 0 ]; then
	echo Please run this as the root user. &>2
	exit 1
fi

# Check if run on Ubuntu 20.04 
. /etc/os-release
if [ "$VERSION_CODENAME" != "focal" ]; then
	echo This script is to be run on Ubuntu 20.04. &>2
	exit 1
fi

# Update cache and install default jdk
apt update
apt install openjdk-11-jdk -y

# Setting up a Tomcat User
useradd -r -m -U -d /opt/tomcat -s /bin/false tomcat

# Downloading the Tomcat package
wget -c https://dlcdn.apache.org/tomcat/tomcat-10/v10.1.5/bin/apache-tomcat-10.1.5.tar.gz

# Install Tomcat on Linux
tar xf apache-tomcat-10.1.5.tar.gz -C /opt/tomcat
ln -s /opt/tomcat/apache-tomcat-10.1.5 /opt/tomcat/updated
chown -R tomcat: /opt/tomcat/*
sh -c 'chmod +x /opt/tomcat/updated/bin/*.sh'

# Configuring the Tomcat service
cat <<EOF > /etc/systemd/system/tomcat.service
[Unit]
Description=Apache Tomcat Web Application Container
After=network.target

[Service]
Type=forking

Environment="JAVA_HOME=/usr/lib/jvm/java-1.11.0-openjdk-amd64"
Environment="CATALINA_PID=/opt/tomcat/updated/temp/tomcat.pid"
Environment="CATALINA_HOME=/opt/tomcat/updated/"
Environment="CATALINA_BASE=/opt/tomcat/updated/"
Environment="CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC"
Environment="JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom"

ExecStart=/opt/tomcat/updated/bin/startup.sh
ExecStop=/opt/tomcat/updated/bin/shutdown.sh

User=tomcat
Group=tomcat
UMask=0007
RestartSec=10
Restart=always

[Install]
WantedBy=multi-user.target
EOF

# Start and enable Tomcat
systemctl daemon-reload
systemctl start tomcat
systemctl enable tomcat

# Delete the contents of webapps dir
rm -rf /opt/tomcat/apache-tomcat-10.1.5/webapps/*
